import { render, waitFor, screen } from '@testing-library/react';
import App from './App';

// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom';

test('table renders with headers', async () => {
  render(<App />);
  await waitFor(() => screen.getByRole('table'))
  const uuidHeader = screen.getByText(/Uuid/g);
  expect(uuidHeader).toBeInTheDocument();
  const nameHeader = screen.getByText(/Name/g);
  expect(nameHeader).toBeInTheDocument();
  const emailHeader = screen.getByText(/Email/g);
  expect(emailHeader).toBeInTheDocument();
  const requestedAmountHeader = screen.getByText(/Requested Amount/g);
  expect(requestedAmountHeader).toBeInTheDocument();
  const paymentAmountHeader = screen.getByText(/Payment Amount/g);
  expect(paymentAmountHeader).toBeInTheDocument();
  const paymentMethodHeader = screen.getByText(/Payment Method/g);
  expect(paymentMethodHeader).toBeInTheDocument();
  const initiatePaymentHeader = screen.getByText(/Initiate Payment/g);
  expect(initiatePaymentHeader).toBeInTheDocument();
});

test('Pay button only available if appliction is found', async () => {
  render(<App />);
  await waitFor(() => screen.getByRole('table'))

  const rows = document.getElementsByTagName('tr')
  //console.log(Object.keys(rows).length)

  for(let i = 0; i < rows.length; i++) {
    //console.log(rows[i])
    console.log(rows[i].children[3].innerHTML)
    console.log()
    if(rows[i].children[6].innerHTML && rows[i].children[3].innerHTML == '') {
      throw new Error('Pay button showing for user with no application')
      return false;
    }
  }

  /*const rows = screen.queryAllByRole('button')
  console.log(rows.length)
  console.log(rows[0])*/
});

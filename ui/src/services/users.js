import axios from "axios";

const userServiceBaseUrl = "http://localhost:8080";

export const getUsers = async () => {
  const { data } = await axios.get(`${userServiceBaseUrl}/users`);
  return data;
};

export const getUsersPaginated = async (start, count) => {
  const { data } = await axios.get(`${userServiceBaseUrl}/usersPaginated?start=${start}&count=${count}`);
  return data;
};
